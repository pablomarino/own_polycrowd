/**
 *  @author Pablo Mariño Boga
 *  @version 0.1
 */


/**
 *  wait for all page elements to load
 */
window.addEventListener("load",eventWindowLoaded,false);

/**
 * Onload event triggers canvas and object creatopm
 */
function eventWindowLoaded(){
    if (Modernizr.canvas) {  // Canvas suppot found

        var timer, datos, pantalla, i, j, numShapes, tmpShape, validShapes;
        //
            numShapes = 1000;
        // start measuring time
            timer = new Date();
        // screen reference
            pantalla = new view(document.getElementById("canvas00"));
        // create data to draw objects
            datos = new elements(pantalla);
        //
            pantalla.init();

        for (i = 0; i < numShapes; i++) {
            // añado rectangulos
            datos.add(datos.generateRect());
        }
datos.pretty();
validShapes = datos.getLength();

        //for(i=0;i<validShapes;i++){
        for (i = 0; i < datos.getLength(); i++){
            for (j = 0; j < datos.getElement(i).length; j++) {
                tmpShape = datos.getElement(i).pop();
                pantalla.drawShape(tmpShape);
            }
        }

        // Devuelvo el tiempo total tras la ejecucion
        console.log("Time: "+((new Date()-timer))+" ms, "+validShapes+" drawn elements, Fit ratio : "+(validShapes/numShapes));
    }else{
        // No hay soporte para canvas
        return;
    }
}

