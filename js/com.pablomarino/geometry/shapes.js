/**
 *  @author Pablo Mariño Boga
 *  @version 0.1
 *  @date 05/11/15
 */
var Point, Line, Rect, Ngon;


/*
*  Todo
*  implement procedures hits,contains,boundingbox, draw boundingbox
*  Ngon
*  rewrite Point ,Line, Rect as subcases of Ngon
* */

Point = function (_x, _y){
    if(!isNaN(_x) && !isNaN(_y)){
        var x,y;

        this.x=Math.round(_x);
        this.y=Math.round(_y);

        Point.prototype.setX = function(_x){
            this.x = _x;
        }

        Point.prototype.setY = function(_y){
            this.y = _y;
        }

        Point.prototype.getX = function(){
            return this.x;
        }

        Point.prototype.getY = function(){
            return this.y;
        }

        Point.prototype.hits =function(_b){
            // TODO
            return false;
        }

        Point.prototype.boundingbox=function(){
            return null;
        }

        Point.prototype.draw = function (_context){
            _context.strokeStyle = "#FEFEFE";
            _context.beginPath();
            _context.moveTo(this.p0.x, this.p0.y);
            _context.lineTo(this.p0.x, this.p0.y);
            _context.stroke();
        }

    }else{
        console.log("Error: Point Creation - Bad arguments.")
    }

}

Line = function (_p1,_p2){
    if(_p1 instanceof Point && _p2 instanceof Point ){

        var p1, p2, boundingbox, draftmode;

        this.p1 = _p1;
        this.p2 = _p2;
        this.draftmode = false;
        this.boundingbox = new Rect(this.p1,this.p2);

        Line.prototype.getP1 = function(){
            return this.p1;
        }


        Line.prototype.getP2 = function(){
            return this.p2;
        }

        Line.prototype.hits =function(_b){
            // TODO
            return false;
        }

        Line.prototype.boundingbox = function(){
            return this.boundingbox;
        }

        Line.prototype.getDraftmode=function(){return draftmode}

        Line.prototype.setDraftmode=function(_b){
                if(typeof(_b) === Boolean){
                    this.draftmode=true;
                }else{
                    console.log("Error: setting draftmode - Bad arguments.");
                }
        }

        Line.prototype.draw = function (_context){
            _context.strokeStyle = randColor();//"#FEFEFE";
            _context.beginPath();
            _context.moveTo(this.p0.x, this.p0.y);
            _context.lineTo(this.p1.x, this.p1.y);
            _context.stroke();
            if(draftmode){
                _context.beginPath();
                _context.moveTo(this.boundingbox.p1.x, this.p1.boundingbox.y);
                _context.lineTo(this.boundingbox.p2.x, this.p1.boundingbox.y);
                _context.lineTo(this.boundingbox.p2.x, this.p2.boundingbox.y);
                _context.lineTo(this.boundingbox.p1.x, this.p2.boundingbox.y);
                _context.lineTo(this.boundingbox.p1.x, this.p1.boundingbox.y);
                _context.stroke();
            }
        }

    }else{
        console.log("Error: Line Creation - Bad arguments.")
    }
}
/**
 * Genera un rectangulo dados dos puntos
 * @param _p1 Punto
 * @param _p2 Punto
 * @constructor
 */
Rect = function (_p1,_p2){
    if(_p1 instanceof Point && _p2 instanceof Point ){

        var p1, p2, tmp, boundingbox, draftmode;

        this.p1 = _p1;
        this.p2 = _p2;
        this.draftmode = false;
        this.boundingbox = this; //TODO check this, does this even work ????????????

        // Sort P1, p2 so P1 is on TL corner and p2 on BR
            if(this.p1.getX()>this.p2.getX()){
                this.tmp = _p1.getX()
                this.p1.setX(_p2.getX());
                this.p2.setX(this.tmp);
            }

            if(this.p1.getY()>this.p2.getY()){
                this.tmp = _p1.getY();
                this.p1.setY(_p2.getY());
                this.p2.setY(this.tmp);
            }

        Rect.prototype.getP1 = function(){return this.p1;}

        Rect.prototype.getP2 = function(){return this.p2;}

        Rect.prototype.hits =function(_b){
            var collides;
            collides = false;

             if(_b instanceof Point){
                // 1 solo punto solo necesita comprobar si esta en el interior del triangulo
                if (
                    _b.getX()>=this.p1.getX() &&
                    _b.getX()<=this.p2.getX() &&
                    _b.getY()>=this.p1.getY() &&
                    _b.getY()<=this.p2.getY()
                ){
                    collides = true;
                }
             }else if(_b instanceof Line){

             }else if(_b instanceof Rect) {
                 if (
                     ( _b.getP1().getX()>=this.p1.getX() &&
                       _b.getP1().getX()<=this.p2.getX() &&
                       _b.getP1().getY()>=this.p1.getY() &&
                       _b.getP1().getY()<=this.p2.getY() )  ||

                     ( _b.getP1().getX()>=this.p1.getX() &&
                       _b.getP1().getX()<=this.p2.getX() &&
                       _b.getP2().getY()>=this.p1.getY() &&
                       _b.getP2().getY()<=this.p2.getY() )  ||

                     ( _b.getP1().getX()>=this.p1.getX() &&
                       _b.getP1().getX()<=this.p2.getX() &&
                       _b.getP1().getY()>=this.p1.getY() &&
                       _b.getP1().getY()<=this.p2.getY() )  ||

                     ( _b.getP2().getX()>=this.p1.getX() &&
                       _b.getP2().getX()<=this.p2.getX() &&
                       _b.getP2().getY()>=this.p1.getY() &&
                       _b.getP2().getY()<=this.p2.getY() )
                 ){
                     collides = true;
                 }
             }else if(_b instanceof Ngon) {

             }
            return collides;
        }

        Rect.prototype.draw = function (_context){
            _context.strokeStyle = randColor();//"#FEFEFE";
            _context.beginPath();
            _context.moveTo(this.p1.x, this.p1.y);
            _context.lineTo(this.p2.x, this.p1.y);
            _context.lineTo(this.p2.x, this.p2.y);
            _context.lineTo(this.p1.x, this.p2.y);
            _context.lineTo(this.p1.x, this.p1.y);
            if(draftmode){
                // do nothing
            }
            _context.stroke();
        }


    }else{
        console.log("Error: Rect Creation - Bad arguments.")
    }
}

/**
 *
 * @param _sides
 * @param _center
 * @param _radius
 */
Ngon = function (_sides,_center,_radius){
    // TODO implement
    if(!isNaN(_sides) && _center instanceof Point && !isNaN(_radius)) {

        var i, sides,radius, vertex, boundingbox, draftmode,tmpP,tmpX,tmpY, tmpAngle;

        this.center = _center;
        this.radius = _radius;
        this.sides = _sides;
        this.tmpAngle =0;
        this.incDegrees=Math.ceil(360/sides);
        this.vertex = new Array();

        for(i=0;i<this.sides;i++){
            tmpX=this.center.getX()+Math.cos(tmpAngle)*this.radius;
            tmpY=this.center.getY()+Math.sin(tmpAngle)*this.radius;
            tmpP = new Point(tmpX,tmpY);
            this.vertex.push(tempP)
            this.tmpAngle+=this.incDegrees;
        }

        this.draftmode = false;
        this.boundingbox = this; //TODO check this, does this even work ????????????

        // Sort P1, p2 so P1 is on TL corner and p2 on BR
        if(this.p1.getX()>this.p2.getX()){
            this.tmp = _p1.getX()
            this.p1.setX(_p2.getX());
            this.p2.setX(this.tmp);
        }

        if(this.p1.getY()>this.p2.getY()){
            this.tmp = _p1.getY();
            this.p1.setY(_p2.getY());
            this.p2.setY(this.tmp);
        }

        Rect.prototype.getP1 = function(){return this.p1;}

        Rect.prototype.getP2 = function(){return this.p2;}

        Rect.prototype.hits =function(_b){
            var collides;
            collides = false;

            if(_b instanceof Point){
                // 1 solo punto solo necesita comprobar si esta en el interior del triangulo
                if (
                    _b.getX()>=this.p1.getX() &&
                    _b.getX()<=this.p2.getX() &&
                    _b.getY()>=this.p1.getY() &&
                    _b.getY()<=this.p2.getY()
                ){
                    collides = true;
                }
            }else if(_b instanceof Line){

            }else if(_b instanceof Rect) {
                if (
                    ( _b.getP1().getX()>=this.p1.getX() &&
                    _b.getP1().getX()<=this.p2.getX() &&
                    _b.getP1().getY()>=this.p1.getY() &&
                    _b.getP1().getY()<=this.p2.getY() )  ||

                    ( _b.getP1().getX()>=this.p1.getX() &&
                    _b.getP1().getX()<=this.p2.getX() &&
                    _b.getP2().getY()>=this.p1.getY() &&
                    _b.getP2().getY()<=this.p2.getY() )  ||

                    ( _b.getP1().getX()>=this.p1.getX() &&
                    _b.getP1().getX()<=this.p2.getX() &&
                    _b.getP1().getY()>=this.p1.getY() &&
                    _b.getP1().getY()<=this.p2.getY() )  ||

                    ( _b.getP2().getX()>=this.p1.getX() &&
                    _b.getP2().getX()<=this.p2.getX() &&
                    _b.getP2().getY()>=this.p1.getY() &&
                    _b.getP2().getY()<=this.p2.getY() )
                ){
                    collides = true;
                }
            }else if(_b instanceof Ngon) {

            }
            return collides;
        }

        Rect.prototype.draw = function (_context){
            _context.strokeStyle = randColor();//"#FEFEFE";
            _context.beginPath();
            _context.moveTo(this.p1.x, this.p1.y);
            _context.lineTo(this.p2.x, this.p1.y);
            _context.lineTo(this.p2.x, this.p2.y);
            _context.lineTo(this.p1.x, this.p2.y);
            _context.lineTo(this.p1.x, this.p1.y);
            if(draftmode){
                // do nothing
            }
            _context.stroke();
        }
    }else {
        console.log("Error: Circle Creation - Bad arguments.")
    }
}