/**
 * Created by pablomarinoboga on 07/11/15.
 */

/**
 *
 * @param _canvas
 */
var view = function(_canvas){
    var context;

    if (_canvas)
        this.context = _canvas.getContext("2d");
    else{
        // No existe el canvas
        return;
    };

    /**
     *
     */
    view.prototype.init = function(){
        // Dibuja el fondo
        this.context.fillStyle="#EDEDED";
        this.context.fillRect(0,0,_canvas.width,_canvas.height);
    };

    /**
     *
     */
    view.prototype.update = function(){};


    view.prototype.drawShape= function(_s){
console.log("draw")
        _s.draw(this.context);
    };

    view.prototype.getCanvas = function(){
        return _canvas;
    };
};

