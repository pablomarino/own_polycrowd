/**
 * Created by pablomarinoboga on 07/11/15.
 */

var elements = function(_view){
    var _elements, i, subDivWidth, subDivHeight, cellWidth,cellHeight;

    subDivWidth = subDivHeight = 4;
    cellWidth = Math.round(_view.getCanvas().width / subDivWidth);
    cellHeight= Math.round(_view.getCanvas().height / subDivHeight);

    // creo la estructura de un array que representa la pantalla en celdas que identifico por filas y columnas
    // para poder comprobar colisiones
    _elements = new Array();

    for(i=0;i<(subDivWidth*subDivHeight);i++){
        _elements[i] = new Array();
    }

    /**
     *
     * @param _e
     */
    elements.prototype.add = function(_e){
        var col, row, pos;
        // TODO por ahora esta montado para rectangulos habra que ver como va con ngons habra que implementar bounding box y con point

        // Nota : Coloco Shape en la celda que corresponde a su P1, podria ser en su punto medio pero necesitaria calcularlo
        // y con celdas grandes y tamaños de forma pequeños esta opcion da buen resultado, en otras configuraciones pordria valer la pena

        col = Math.floor(_e.getP1().getX()/cellWidth);
        row = Math.floor(_e.getP1().getY()/cellHeight);
        pos = row * subDivWidth + col;

        // compruebo si es valido, si es valido lo añado al array
        if(_elements[pos].length==0){
            _elements[pos].push(_e);
        }else{
            for(i=0;i<_elements[pos].length;i++){
                if(_elements[pos][i].hits(_e))
                    // se detecta una colision
                    break;
                else
                    // no hay colisiones
                    _elements[pos].push(_e);
            }
        }
    }

    /**
     *
     * @returns {T}
     */
    elements.prototype.remove = function(){
        return _elements.pop();
    }

    /**
     *
     * @param _n
     * @returns {*}
     */
    elements.prototype.getElement = function(_n){
        return _elements[_n];
    }

    /**
     *
     * @returns {Number}
     */
    elements.prototype.getLength = function(){
        return _elements.length;
    }

    elements.prototype.generatePoint = function(){
        var s = new Point (rand(0,_canvas.width),rand(0,_canvas.height));
        return s;
    };

    elements.prototype.generateLine =
        elements.prototype.generateRect = function(){
        var s = new Rect(
            new Point (rand(0,_view.getCanvas().width),rand(0,_view.getCanvas().height)),
            new Point (rand(0,_view.getCanvas().width),rand(0,_view.getCanvas().height))
        );
        return s;
    };

    elements.prototype.pretty = function() {
        var x, y, tmpTxt;
        tmpTxt = "";

        for (x = 0; x < _elements.length; x++) {
            tmpTxt += "#" + x + "#|"
            for (y = 0; y < _elements[x].length; y++) {
                tmpTxt += y + "|"
            }
            tmpTxt +="\n"
        }
        console.log(tmpTxt);
    }

}
